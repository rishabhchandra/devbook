type FolderType = {
    id: string;
    name: string;
    description: string;
    ownerId: number;
    createdTime: number;
    files: FolderType[];
}



export default FolderType;