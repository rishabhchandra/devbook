import './styles/App.css';
import {Button} from '@material-ui/core'

function App() {

  const clickHandler = ()=>{
    alert('YES!');
  }

  return (
    <div className="app-root">
      <h1>Welcome to DevBook.</h1>
      <Button variant="contained" color="primary" onClick={clickHandler}>
         Added Material UI?
      </Button>
    </div>
    
  );
}

export default App;
