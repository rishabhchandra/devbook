import React from 'react';
import ReactDOM from 'react-dom';
import './styles/index.css';
import App from './App';
import MainView from './MainView/MainView';
import { BrowserRouter, Redirect } from 'react-router-dom';

ReactDOM.render(
  <React.StrictMode>
    <BrowserRouter>
      <Redirect exact from="/" to="/root" />
      <MainView />
    </BrowserRouter>
  </React.StrictMode>,
  document.getElementById('root')
);

