import TopView from './components/TopView';
import ContentView from './components/ContentView';
import PathNavigation from './components/PathNavigation';
import './MainView.css';
import { useEffect, useState } from 'react';
import { Button, Typography } from '@material-ui/core';
import useLocalStorage from '../utilities/useLocalStorage';
import FolderType from '../utilities/FolderType';
import {useLocation, Link} from 'react-router-dom';
import _ from 'lodash';

function getCurrentFolder(oldFile: FolderType, pathname: string){
  const dir:string[] = pathname.split('/');
  let currentFolder:FolderType = oldFile;
  for(let i=1;i<dir.length;++i){
      if(dir[i]==="") continue;
      for(let o of currentFolder.files){
          if(o.name === dir[i]){
              currentFolder = o;
              break; 
          }
      }
  }
  return currentFolder;
}



function MainView() {
  
  const location = useLocation();
  const [NFopen, setNFopen] = useState(false);
  const [path, setPath] = useState('/root');
  const [currentFolder, setCurrentFolder] = useState<FolderType>();

  const [database, setDatabase] = useLocalStorage<FolderType>('directory', {
    id:'0',
    name:'root',
    description:'root folder',
    ownerId:0,
    createdTime:0,
    files:[]
  });
  
  useEffect(()=>{
    setPath(location.pathname);
    setCurrentFolder(getCurrentFolder(database, location.pathname));
  },[location.pathname, database]);


  const createNewFolder = (folderName: string, folderDescription: string, pathName: string) => {
    const newFolder: FolderType = {
      id: Date.now().toString(),
      name: folderName,
      description: folderDescription,
      ownerId: 1,
      createdTime: Date.now(),
      files:[]
    };
    const newdatabase = _.cloneDeep(database);
    const cf = getCurrentFolder(newdatabase, pathName);
    cf.files.push(newFolder);
    setDatabase(newdatabase);
  }

  function dialogCloser(){
    console.log("modal close");
    setNFopen(false);
  }
  function dialogOpener(){
    console.log("modal open");
    setNFopen(true);
  }
  return (
      <div id="mainview-root">
        <TopView path={path} dialogOpen={NFopen} dialogCloser={dialogCloser} dialogOpener={dialogOpener} folderCreator={createNewFolder}/>
        <hr className="divider"/>
        <PathNavigation directory={path}/>
        <ContentView  directory={currentFolder?.files} pathName={path}/>
      </div>    
  );
}

export default MainView;
