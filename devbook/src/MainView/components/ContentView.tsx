import {Paper, Typography, Grid} from '@material-ui/core'
import FolderView from './FolderView';
import './ContentView.css';
import { ReactElement } from 'react';
import {useLocation, Link, useHistory} from 'react-router-dom';
type FolderType = {
    id: string;
    name: string;
    description: string;
    ownerId: number;
    createdTime: number;
  }

function ContentView({directory, pathName} : {directory: FolderType[]|undefined, pathName:string} ) {
    const history = useHistory();
    return (
        <div className="content-view">
           <Grid container spacing={3}  >
                {
                directory?.map((element)=>{
                    return (
                        <Grid key={element.id} item xs={12} sm={4} md={3} lg={2} >
                            <FolderView folder={element} clickHandler={()=>{
                                history.push(`${pathName}/${element.name}`);
                            }}/>
                        </Grid>
                    );
                })
                }
           </Grid>
        </div>
    );
  }
  
  export default ContentView;
  