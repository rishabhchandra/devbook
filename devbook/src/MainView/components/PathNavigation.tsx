import { Button } from "@material-ui/core";
import {useHistory} from 'react-router-dom';
import NavigateNext from '@material-ui/icons/ArrowForwardIos';
import './PathNavigation.css';


function renderButton(name:string, historyAPI:any,path:string){
    return (<>
        <Button onClick={()=>{
            historyAPI.push(path);
        }}>{name}</Button>
        <div className="icon">
         <NavigateNext/>
        </div>
         
        </>
    )
}


function PathNavigation({directory}:{directory: string}) {
    const history = useHistory();
    const pathList = directory.split('/');
    pathList.shift();
    let prefixPath = "";
    return (
        <div className="navContainer">
        {
            pathList.map((e)=>{
                prefixPath = prefixPath + '/' + e;
                return renderButton(e,history, prefixPath);
            })
        }
        </div>
        
    );
}

export default PathNavigation;
