import { makeStyles, Typography, Icon, Dialog, DialogTitle, DialogContent, DialogActions, Button, TextField } from '@material-ui/core';
import Paper from '@material-ui/core/Paper';
import folderOpenSVG from './../../assets/folder-open.svg';
import './FolderView.css';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import { useState } from 'react';


type FolderType = {
    id: string;
    name: string;
    description: string;
    ownerId: number;
    createdTime: number;
  }

const useStyles = makeStyles(() =>
  ({
  textfield: {
      marginTop:'5%',
  },
  dialogContent: {
    display:'flex',
    flexDirection:'column',
  }
})
);


function FolderView({folder, clickHandler}:{folder: FolderType, clickHandler: ()=>void}){

    const classes = useStyles();

 
    const [edit, setEdit] = useState(false);
    const [showInfo, setShowInfo] = useState(false);

    const infoModal = () => { return (
     <Dialog open={showInfo} onClose={()=>setShowInfo(false)} fullWidth={true}>
        <DialogTitle>Folder Info</DialogTitle>
        <DialogContent className={classes.dialogContent}>
            <TextField className={classes.textfield} variant="outlined" label="Name" value={folder.name} disabled></TextField>
            <TextField className={classes.textfield} variant="outlined" label="Description" multiline value={folder.description} disabled></TextField>
            <TextField className={classes.textfield} variant="outlined" label="Folder ID" value={folder.id} disabled></TextField>
            <TextField className={classes.textfield} variant="outlined" label="Owner ID" value={folder.ownerId} disabled></TextField>
            <TextField className={classes.textfield} variant="outlined" label="Created At" value={new Date(folder.createdTime)} disabled></TextField>
        </DialogContent>
        <DialogActions>
            <Button onClick={()=>setShowInfo(false)}>Close</Button>
        </DialogActions>
    </Dialog>
    );
    }



    return (
    <div className="container" onMouseEnter={()=>setEdit(true)} onMouseLeave={()=>setEdit(false)} onDoubleClick={clickHandler}  >
        <img src={folderOpenSVG} className="folderIcon" alt="folderIcon"></img>
        <div className="box-text">
            <Typography variant="body1">{folder.name}</Typography>
            <Typography variant="caption">{folder.id}</Typography>
        </div>
        { edit && <div onClick={()=>{setEdit(false); setShowInfo(true)}} className="editButton"><MoreVertIcon/></div>}
        {showInfo && infoModal()}        
    </div>
    );
}

export default FolderView;