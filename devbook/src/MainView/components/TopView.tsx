import './TopView.css';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import InputBase from '@material-ui/core/InputBase';
import IconButton from '@material-ui/core/IconButton';
import SearchIcon from '@material-ui/icons/Search';
import Button from '@material-ui/core/Button';
import React, { ReactElement, useState } from 'react';
import { Dialog, DialogActions, DialogContent, DialogTitle, TextField } from '@material-ui/core';


const useStyles = makeStyles(() =>
    ({
    root: {
      padding: '2px 4px',
      display: 'flex',
      alignItems: 'center',
      width: 600,
    },
    input: {
      marginLeft: '5px',
      flex:1,
    },
    iconButton: {
      padding: '5px',
    },
    dialogContent: {
      display:'flex',
      flexDirection:'column',
    }
  })
);

type Error = {
  status:boolean,
  msg: string | null
}

const nameRegex = /^[a-z ,.'-]+$/i;
const descRegex = /^(.|\s)*[a-zA-Z]+(.|\s)*$/;

function TopView({dialogOpen,path, dialogCloser, dialogOpener, folderCreator}:{
  dialogOpen: boolean,
  path:string,
  dialogCloser: ()=>void,
  dialogOpener:()=>void,
  folderCreator: (folderName:string, folderDescription:string, pathName:string)=>void
}):ReactElement {
    const classes = useStyles();

    const [formName, setFormName] = useState('');
    const [formDescription, setFormDescription] = useState('');
    const [fNameError, setFNameError] = useState<Error>({
      status:false,
      msg:null,
    });
    const [fDescError, setFDescError] = useState<Error>({
      status:false,
      msg:null
    });
    

    function submitHandler(){
        if(nameRegex.test(formName)===false || descRegex.test(formDescription)===false){
          if(nameRegex.test(formName)===false){
             setFNameError({
              status:true,
              msg:'Enter valid name.'
            })
          }else{
            setFNameError({
              status:false,
              msg:null,
            });
          };
          if(descRegex.test(formDescription)===false){
            setFDescError({
              status:true,
              msg:'Enter valid description.'
            });
          }else{
            setFDescError({
              status:false,
              msg:null,
          })
          }
        }else{
          folderCreator(formName, formDescription, path);
          cancelHandler();
        }
    }

    function cancelHandler(){
        setFNameError({
            status:false,
            msg:null,
        });
        setFDescError({
            status:false,
            msg:null,
        })
        setFormName('');
        setFormDescription('');
        dialogCloser();    
    }

    return (
        <div className="topview-root">
            <Paper className={classes.root}>
                <InputBase
                    className={classes.input}
                    placeholder="Search Directory"
                    onKeyDown={(event)=>{
                        if(event.key === 'Enter'){
                            alert("Enter Pressed.");
                        }
                    }}
                />
                <IconButton onClick={()=>{alert('clicked.')}} className={classes.iconButton} >
                    <SearchIcon />
                </IconButton>
            </Paper>
            <Button variant="contained" color="primary" onClick={dialogOpener}>Create Folder</Button>
            <Dialog open={dialogOpen} onClose={cancelHandler} fullWidth={true}>
                <DialogTitle>Create New Folder</DialogTitle>
                <DialogContent className={classes.dialogContent} >
                  <TextField error={fNameError.status} helperText={fNameError.msg} value={formName} onChange={(e)=>setFormName(e.target.value)} id="folderNameInput" label="Folder Name" variant="outlined" required/>
                  <TextField error={fDescError.status} helperText={fDescError.msg} value={formDescription} onChange={(e)=>setFormDescription(e.target.value)} id="folderDescriptionInput" label="Folder Description" variant="outlined" margin="normal" rows="5" multiline required/>
                </DialogContent>
                <DialogActions>
                <Button autoFocus color="primary" onClick={cancelHandler}> 
                  Cancel
                </Button>
                <Button color="primary" onClick={submitHandler}>
                  Create
                </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
  }
  
  export default TopView;
  